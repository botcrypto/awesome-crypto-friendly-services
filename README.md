# crypto friendly services

## Installation

### Requirements

- [Nodejs](https://nodejs.org/en/) (used version 12.18.3)

### Installation steps

- `npm i` to install dependencies.

- `npm run serve` to launch a development server for the app.

- `npm run load_GS_data` to compile and launch Google Sheets fetching data script. (can only be used with the environment variable "G_API_KEY".)

### Environment variables

VUE_APP_MATOMO_URL
VUE_APP_MATOMO_ID
VUE_APP_G_DOC_ID
VUE_APP_G_API_KEY (script only)