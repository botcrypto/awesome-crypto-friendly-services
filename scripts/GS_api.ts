import { GoogleSpreadsheet } from "google-spreadsheet";
const dotenv = require("dotenv");

dotenv.config();

interface field {
  Name: string;
  Category: string;
  Cryptocurrency: string;
  CryptocurrencyTrading: string;
  ICO: string;
  Extract: string;
  ExtractLink: string;
}

interface row {
  _rowNumber: number;
  Name: string;
  Category: string;
  Cryptocurrency: string;
  CryptocurrencyTrading: string;
  ICO: string;
  Extract: string;
  ExtractLink: string;
}

async function GetData() {
  // spreadsheet key is the long id in the sheets URL
  const doc = new GoogleSpreadsheet(process.env.VUE_APP_G_DOC_ID);
  doc.useApiKey(process.env.VUE_APP_G_API_KEY);

  await doc.loadInfo(); // loads document properties and worksheets

  const sheet = doc.sheetsByIndex[0]; // or use doc.sheetsById[id]

  const rows = await sheet.getRows();

  let data_json = [];
  let field: field;
  rows.forEach((element: row) => {
    if (element._rowNumber == 0) {
      // ignore headers
    } else {
      field = {
        Name: element.Name,
        Category: element.Category,
        Cryptocurrency: element.Cryptocurrency,
        CryptocurrencyTrading: element.CryptocurrencyTrading,
        ICO: element.ICO,
        Extract: element.Extract,
        ExtractLink: element.ExtractLink,
      };
      data_json.push(field);
    }
  });

  const fs = require("fs");
  console.log("start writing data");
  fs.writeFile(
    "./src/data/data.json",
    JSON.stringify(data_json),
    (err: string) => {
      if (err) {
        throw err;
      }
      console.log("JSON data is saved.");
    }
  );
}

GetData();
