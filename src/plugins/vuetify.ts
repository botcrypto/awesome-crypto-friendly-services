import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

import en from 'vuetify/src/locale/en';
import fr from 'vuetify/src/locale/fr';

export default new Vuetify({
  lang: {
    locales: { en, fr },
    current: 'fr',
  },
});
