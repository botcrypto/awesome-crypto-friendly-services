import fr from "./fr.json";
import en from "./en.json";
import VueI18n from "vue-i18n";
import Vue from "vue";

Vue.use(VueI18n);

export default new VueI18n({
  locale: localStorage.getItem("lang") || "en",
  messages: {
    en,
    fr,
  },
});
