import Vue from "vue";
import vuetify from "./plugins/vuetify";
import App from "./App.vue";
import i18n from "./translations/i18n";
Vue.config.productionTip = false;

new Vue({
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
